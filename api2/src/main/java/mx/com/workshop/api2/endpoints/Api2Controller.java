package mx.com.workshop.api2.endpoints;

import mx.com.workshop.commons.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/api2")
public class Api2Controller {

    @GetMapping
    public ApiResponse index() throws InterruptedException {
        Random random = new Random();
        long sleepingTime = random.nextInt(1000) * 100000L;
        long time2 = sleepingTime;

        while(sleepingTime > 0) sleepingTime--;

        return buildResponse(time2);
    }

    private ApiResponse buildResponse(long time) {
        ApiResponse api2Response = new ApiResponse();
        api2Response.setStatusCode(200);
        api2Response.setField1("Api2 field");
        api2Response.setField2("api2 2 field");
        api2Response.setField3("api2 3 field");
        api2Response.setField4("api2 4 field");
        api2Response.setTime(time);

        return api2Response;
    }
}
