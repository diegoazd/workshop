package mx.com.workshop.api1.endpoints;

import mx.com.workshop.commons.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/api1")
public class ExampleController {

    @GetMapping
    public ApiResponse index() throws InterruptedException {
        Random random = new Random();
        long sleepingTime = random.nextInt(6000) * 100000L;
        long time2 = sleepingTime;

        while(sleepingTime > 0) sleepingTime--;

       return buildApiResponse(time2);
    }

    private ApiResponse buildApiResponse(long time) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setField1("field1");
        apiResponse.setField2("field2");
        apiResponse.setField3("field3");
        apiResponse.setField4("field4");
        apiResponse.setTime(time);
        apiResponse.setStatusCode(200);

        return apiResponse;
    }
}
