package mx.com.worksop.orchestrate.service;

import mx.com.workshop.commons.ApiResponse;
import mx.com.worksop.orchestrate.clients.Api1Client;
import mx.com.worksop.orchestrate.clients.Api2Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrchestrateService {

  @Autowired
  Api1Client api1Client;

  @Autowired
  Api2Client api2Client;

  public String fetchApis() {
    ApiResponse api1 = api1Client.fetch();

    if(api1.getStatusCode() != 200) {
      return "Error";
    }

    ApiResponse api2 = api2Client.fetch();

    return "Api1 time: " + String.valueOf(api1.getTime()) + " ,api2 time: "+
        String.valueOf(api2.getTime());
  }
}
