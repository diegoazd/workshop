package mx.com.worksop.orchestrate.endpoints;

import mx.com.worksop.orchestrate.clients.Api1Client;
import mx.com.worksop.orchestrate.service.OrchestrateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orchestrate")
public class OrchestrateController {

    @Autowired
    OrchestrateService orchestrateService;

    @GetMapping
    public String index() {
        return orchestrateService.fetchApis();
    }
}
