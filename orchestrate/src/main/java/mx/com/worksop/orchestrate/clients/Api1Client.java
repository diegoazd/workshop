package mx.com.worksop.orchestrate.clients;

import mx.com.workshop.commons.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Component
public class Api1Client {

  @Autowired
  WebTarget webTargetApi1;

  public ApiResponse fetch() {
    return webTargetApi1.path("api1/")
        .request(MediaType.APPLICATION_JSON)
        .get(ApiResponse.class);
  }
}
