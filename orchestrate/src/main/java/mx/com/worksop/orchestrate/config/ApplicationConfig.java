package mx.com.worksop.orchestrate.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Configuration
public class ApplicationConfig {

  @Bean
  WebTarget webTargetApi1() {
    return ClientBuilder.newBuilder().build().target("http://localhost:8086/");
  }

  @Bean
  WebTarget webTargetApi2() {
    return ClientBuilder.newBuilder().build().target("http://localhost:8087/");
  }
}
